﻿using HMS.UIRepository.WPFByDK.Lib.BizWindow;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace HMS.UIRepository.WPFByDK.UC
{ 
    public class CornerColoredButton : System.Windows.Controls.Button
    {
        //黄金分割算法
        const float GSlicer = 0.875F;
        static CornerColoredButton()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(CornerColoredButton), 
                new FrameworkPropertyMetadata(typeof(CornerColoredButton)));
        }
        void resizeCallBack(object sender)
        {
            if (sender is CornerColoredButton btn)
            {
                var AH = Math.Max(ActualHeight, ActualWidth);
                btn.FontSize = AH * GSlicer;
                btn.FontWeight = FontWeight.FromOpenTypeWeight(12);
                while (btn.FontSize * btn.Content.ToString().Length > AH * GSlicer)
                {
                    btn.FontSize *= GSlicer;
                }
            }
        }
        public CornerColoredButton()
        {
            /* 根据该控件的在父类中的宽高ActualHeight 动态计算 边框和字体的自适应尺寸 */
            this.Loaded += (sender, e) => {
                resizeCallBack(sender);
            };
            this.Click += (sender, e) => {
                var window = new ReportPubs4SalesWinow();
                WPFTool.ScaleTheScreen(window);
                window.Show();
            };
        }
        protected override void OnRenderSizeChanged(SizeChangedInfo sizeInfo)
        {
            base.OnRenderSizeChanged(sizeInfo);
            resizeCallBack(this);
        }
    }
}
