﻿using System;
using System.Windows.Media;
using System.Windows;
using System.Windows.Controls;

namespace HMS.UIRepository.WPFByDK
{
    /// <summary>
    /// WPF工具类 WCT
    /// </summary>
    public static partial class WPFTool
    {
        /* WPF - 扩展 FrameWorkElement 随机色笔刷 brushes*/
        public static Random RdSeed = new Random();

        /* 随机像素色 */
        public static Brush DKBrush {
            get
            {
                var colorRandom = Color.FromScRgb(1.0F,
                RdSeed.Next(32,Byte.MaxValue) / 255,
                RdSeed.Next(64, Byte.MaxValue) / 255,
                RdSeed.Next(128, Byte.MaxValue) / 255);

                return new SolidColorBrush(colorRandom);
            }
        }

        // 0.获取屏幕尺寸 宽高*0.8F 并且居中
        // 1.将一个 WPF窗体(Window)的位置居中
        public static void ScaleTheScreen(Window frame)
        {
            frame.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            frame.FontSize = 14.00F;
            frame.Width = WPFTool.ScreenSize.Width * 0.8;
            frame.Height = WPFTool.ScreenSize.Height * 0.8;
            frame.Background = Brushes.WhiteSmoke;
        }
        
        // 获取工作区尺寸
        public static Size ScreenSize
        {
            get
            {
                return SystemParameters.WorkArea.Size;
            }
        }
    }
}
