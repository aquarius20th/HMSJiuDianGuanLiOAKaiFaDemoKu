﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HMS.UIRepository.WPFByDK.ViewModel
{
    public enum TemplateOrientation
    {
        LeftToRight = -1 ,
        TopToLeft = -2 ,
        TopToLeftAndRight = -3,
        CornerRadius = -4
    }
    // MVVM - 数据模型
    public class FuncBtnVM
    {
        /*
         * 定义一个数据模型 提供给`系统按钮`使用 一般来说 功能按钮都有 1.一个图标 2.一行文字 
         * 远程请求 Uri (https://www.baidu.com/img/bd_logo1.png) 3.底层图标更新需要用到的
         */
        internal protected readonly TemplateOrientation Direction;
        internal protected Uri _remoteURI = new Uri("https://www.baidu.com/img/bd_logo1.png");

        public string FuncTitle { get; set; }
        public string FuncIMGResPath { get; set; }
        public string RemoteIMGUriAddr {
            get {
                return _remoteURI.AbsolutePath;
            }
            set {
                _remoteURI = new Uri(value);
            }
        }
    }
}
