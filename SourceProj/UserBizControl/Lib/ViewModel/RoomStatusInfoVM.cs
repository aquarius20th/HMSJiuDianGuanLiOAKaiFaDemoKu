﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace HMS.UIRepository.WPFByDK.ViewModel
{
    /// <summary>
    /// 用于 系统Windows所有的button可悬浮空间生成
    /// 所需要用到的实体类 通常来说都是交给 自定义数据模板的ToolTip元素
    /// 但我觉得也可以封装成100%代码实现的 Grid自定义控件
    /// 组件由2个部分构成 
    /// 1.数据实体 RSI.cs 2.数据模板DataTemplate(XAML) or 2.自定义用户控件(.CS)
    /// </summary>
    public class RoomStatusInfoVM
    {
        // 房间号(双人房)
        public string RoomNoWithTypeName { get; set; }
        // 客房单号
        public string RoomOrderNo { get; set; }
        // 客人姓名
        public string OwnerPerson { get; set; }    
        // 协议单位
        public string OwnerWorkUnit { get; set; } 
        // Checkin 时间
        public DateTime CheckInDate { get; set; }
        // CheckOut 时间 
        public DateTime CheckOutDate { get; set; }
    }

    public class Ownerinfo
    {
        [Description("订房人姓名 注意不一定是入住人")]
        public string OrderBuyerName { get; set; }
        [Description("订房人电话 注意不一定是入住人")]
        public string OrderBuyerContact { get; set; }
    }

    /// <summary>
    /// 今日房价.号牌信息
    /// </summary>
    public class RoomServicePriceInfo
    {
        [Description("当日房价")]
        public double daylyPrice { get; set; }
        [Description("押金总额")]
        public double garantyVal { get; set; }
        [Description("预授权")]
        public string PrePayMent { get; set; }
        [Description("客人余额")]
        public double GuaranteeRested { get; set; }
        [Description("累计消费")]
        public double GuaranteeUsed { get; set; }
    }

    public class NotePadNS
    {
        // 客人备注信息
        private string info;
        public string GuestInfoNotesText {
            get {
                return @"
从明后而嬉游兮，登层台以娱情。
见太府之广开兮，观圣德之所营。
建高门之嵯峨兮，浮双阙乎太清。
立中天之华观兮，连飞阁乎西城。
临漳水之长流兮，望园果之滋荣。
仰春风之和穆兮，听百鸟之悲鸣。
天云垣其既立兮，家愿得而获逞。
扬仁化于宇内兮，尽肃恭于上京。
惟桓文之为盛兮，岂足方乎圣明。";
            }
            set {
                info = value;
                
            }
        }
    }
}
