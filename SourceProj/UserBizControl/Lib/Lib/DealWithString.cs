﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

/// <summary>
/// Author.Authority - DimChumHaul 丁诚昊
/// </summary>
namespace DrDimsLib.CommonTool.Extensions
{
    using System.Text.RegularExpressions;

    [Description("字符串常用处理函数库 - 丁诚昊")]
    public static partial class DealWithString
    {
        public static bool isTxtValid(this string oriStr)
        {
            // 字符串指针不为空且字面值不为空
            return !string.IsNullOrEmpty(oriStr) &&
                   !string.IsNullOrWhiteSpace(oriStr);
        }

        /// <summary>
        /// 去除重复
        /// </summary>
        /// <param name="value">值，范例1："5555",返回"5",范例2："4545",返回"45"</param>
        public static string Distinct(this string oriStr)
        {
            // 调用 系统的 Enumerable(Linq扩展方法)进行`字符串去重`
            return new string(oriStr.ToCharArray().Distinct().ToArray());
        }

        // 单词计数统计
        public static Dictionary<string, int> CountWords(this string Content)
        {
            string[] words = Regex.Split(Content, @"\W+");
            Dictionary<string,int> docker = new Dictionary<string, int>();
            foreach(var item in words)
            {
                if (docker.ContainsKey(item))
                    docker[item]++;
                else
                    docker[item] = 1;
            }
            return docker;
        }
    }
}
