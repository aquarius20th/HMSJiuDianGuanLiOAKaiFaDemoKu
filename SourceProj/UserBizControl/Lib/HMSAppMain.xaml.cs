﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Threading;

namespace HMS.UIRepository.WPFByDK
{
    using UC;
    using ViewModel;
    using System.Windows.Threading;
    using DrDimsLib.CommonTool.Extensions;
    using System.ComponentModel;

    /// <summary>
    /// MainWindow.xaml 的交互逻辑
    /// </summary>
    public partial class HMSAppMain : Window, INotifyPropertyChanged
    {
        #region 绑定属性
        private string _titleString;
        public string TestTextString
        {
            get
            {
                return _titleString;
            }
            set
            {
                _titleString = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("TestTextString"));
            }
        }
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion

        #region 一些常用UI全局变量
        const float GlobalRoomViewMargin = 5.0F;
        const float RoomBlockMargin = 12.55F;
        const float RoomBlockRadius = 64.00F;

        private string[] dataSourceTitles = new string[] {
            "东莞太子酒店","艾泽拉斯","大床房","小床房不带厕所",
            "差不多就可以了","根据文字数量自动计算大小-7行代码写完 还有4个大括号"};
        // 通用测试字符串
        #endregion

        public HMSAppMain()
        {
            // 设置自身 frame主窗体矩阵
            InitializeComponent();

            // 初始化右边的 WrapPanel
            InitializeRightConponent();

            Loaded += loadedRun;
            TestTextString = "勿在浮沙筑高台";

            // 网上下文中直接丢变量
            DataContext = this;

            var Model = new ViewModel.RoomStatusInfoVM
            {
                RoomNoWithTypeName = "9602（双人房）",
                RoomOrderNo = "55",
                OwnerPerson = "丁诚昊",
                OwnerWorkUnit = "成都网顺智能软件 研发组",
                CheckInDate = DateTime.Now,
                CheckOutDate = DateTime.Now + TimeSpan.FromDays(2),
            };

            this.popingView5.Content = Model;
            this.popingView2.Content = new ViewModel.NotePadNS();
            this.RoomServiceBlock.Content = new ViewModel.RoomServicePriceInfo
            {
                daylyPrice = 498.00F,
                garantyVal = 1200.00F,
                PrePayMent = "不知道什么鬼~",
                GuaranteeRested = (1200.00 - 498.00F),
                GuaranteeUsed = 498.00F,
            };
            this.contactBlock.Content = new Ownerinfo
            {
                OrderBuyerName = "黄鹤",
                OrderBuyerContact = "没有预留电话",
            };
        }

        private void loadedRun(object sender, RoutedEventArgs e)
        {
            // 居中 获取屏幕宽高
            WPFTool.ScaleTheScreen(this);
            Console.WriteLine("生命周期方法 :[Loaded 完成加载]~ ");
            Console.WriteLine("输出自己的真实宽高 [{0}-{1}]", this.ActualWidth, this.ActualHeight);
            // 那么所有的布局方法都应该放在这里
            // InitializeTheAppHomePage();
        }

        private void InitializeRightConponent()
        {
            var view = this.rightView;
            view.Orientation = Orientation.Horizontal;
            for (int i = 0; i < 7; i++)
            {
                var cornerButton = new CornerColoredButton();
                cornerButton.Style = FindResource("CorButtonStyle") as Style;
                cornerButton.Content = dataSourceTitles[i % (dataSourceTitles.Length)];
                view.Children.Add(cornerButton);
            }
        }

        private void contentView_Click(object sender, RoutedEventArgs e)
        {
            if (sender is Button btn)
            {
                TestTextString = "Bind双向绑定成功!";
                new Thread(new ParameterizedThreadStart(GetResult)).Start();
            }
        }
        private void GetResult(object inputNumber)
        {
            Thread.Sleep(3000);
            this.Dispatcher.BeginInvoke((Action)delegate ()
            {
                TestTextString = TestTextString.ToBase64String();
            });
        }

        // 2.初始化 `酒店房态区`
        //public void InitializeTheAppHomePage()
        //{
        //    // 设置 ListBox 距离右边一定的距离 用于布局 最左边的 TabItemControl
        //    rightView.Margin = new Thickness(RoomBlockMargin);
        //    rightView.FlowDirection = FlowDirection.LeftToRight;
        //    for (int i = 0; i < 232; i++)
        //    {
        //        Border box = new Border
        //        {
        //            Background = WPFTool.DKBrush,
        //            ClipToBounds = true,
        //            MinWidth = 44,
        //            MinHeight = 44,
        //        };
        //        rightView.Children.Add(box);
        //    }
        //    /* 
        //     * 获取当前容器的整体宽高 然后进行切割计算 
        //     * 可以知道的是 首页应该有 5行 13列 --- 原型图
        //     * 但是每一个房间格子 应该有一个最小的宽高值 不会随着缩放而变小
        //     */
        //    void CalcSizeAsync()
        //    {
        //        // 测试异步获取 尺寸
        //        Thread.Sleep(150 * WPFTool.RdSeed.Next(3, 8));
        //        this.Dispatcher.BeginInvoke(new Action(() =>
        //        {
        //            int rowNum = 13, ColNum = 13;
        //            Size viewSize = rightView.RenderSize;
        //            var H = (viewSize.Height - 2 * RoomBlockMargin - rowNum * GlobalRoomViewMargin) / rowNum;
        //            var W = viewSize.Width / ColNum;
        //            var raduis = (float)(Math.Min(H, W));
        //            var title = string.Format("异步获取后台尺寸 宽度{0}--高度{1}", viewSize.Width, viewSize.Height);
        //            Title = title;
        //            // 异步更新UI
        //            foreach (ListBoxItem item in rightView.Children)
        //            {
        //                item.MinWidth = raduis;
        //                item.MinHeight = raduis;
        //            }
        //        }));
        //    }
        //    Thread T = new Thread(CalcSizeAsync);
        //    T.Start();
    }
}
