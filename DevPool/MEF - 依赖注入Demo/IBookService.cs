﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MEF___依赖注入Demo
{
    public interface IBookService
    {
        string BookName { get; set; }
        string GetBookName();
        IEnumerable<string> GetBookByAuthor(string keyAuthor = "余秋雨");
    }
}
