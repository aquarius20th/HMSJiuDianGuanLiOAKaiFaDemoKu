﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace MEF___依赖注入Demo
{
    using MyLib = MEF___依赖注入Demo;

    // MEF 主体框架程序集
    using System.ComponentModel.Composition;
    using System.ComponentModel.Composition.Hosting;
    using System.Reflection;

    class Program
    {
        [Import()]
        public IBookService Service { get; set; }
        static void Main(string[] args)
        {
            Lazy<Program> runner = new Lazy<Program>();
            runner.Value.Compose();
            if (runner.Value.Service != null)
            {
                var title = "MEF依赖注入 ---> ";
                Console.WriteLine(title);
                runner.Value.Service.GetBookByAuthor()
                 .ToList()
                 .ForEach(item =>
                 {
                    Console.WriteLine(item);
                 });
            }

            while (true) ;
        }

        private void Compose()
        {
            var ASM = new AssemblyCatalog(Assembly.GetExecutingAssembly());
            CompositionContainer docker = new CompositionContainer(ASM);
            docker.ComposeParts(this);
        }
    }
}
