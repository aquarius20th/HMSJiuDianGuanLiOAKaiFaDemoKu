﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MEF___依赖注入Demo
{
    using System.ComponentModel.Composition;

    [Export(typeof(IBookService))]
    public class FictionBookService :IBookService
    {
        public string BookName { get; set; }

        public string GetBookName()
        {
            return "<行者无疆>";
        }

        public IEnumerable<string> GetBookByAuthor(
            string keyAuthor = "余秋雨") {
            return new string[] {
                "<文化苦旅>","<中国文脉>","<君子之道>",
                "<何谓文化>","<冰河>","<千年一叹>","<泥步修行>"
            };
        }
    }
}
