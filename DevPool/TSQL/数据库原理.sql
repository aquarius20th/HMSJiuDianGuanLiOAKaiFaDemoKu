
-- 外连接 取出234的最小值 为rows条数
select * from sales sa left join stores as st 
	on sa.stor_id = st.stor_id join discounts as dt on sa.stor_id = dt.stor_id
-- 内连接 取出234的最小值 为rows条数
select * from sales as A , stores as B , discounts as C 
where A.stor_id = B.stor_id and A.stor_id = C.stor_id
-- 子查询
select * from discounts as C where C.stor_id in 
(select A.stor_id from sales as A , stores as B where A.stor_id = B.stor_id);
 