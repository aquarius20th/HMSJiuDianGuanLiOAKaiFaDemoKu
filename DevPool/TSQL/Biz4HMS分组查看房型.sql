﻿-- link 1.roomModel(price) + 2.hotelRoom(RoomType) char 
select 
	min(B.RoomTypeID)	房型编号,	-- 房型编号
	max(A.ModelName)	房型描述,	-- 房型描述
	max(B.BasePrice)	最高价格,	-- 房型价格
	count(A.ModelName)	房型数量  	-- 房型数量
		from HMS_RoomModel A right join HMS_HotelRoom B on A.ID = B.RoomTypeID
		group by B.RoomTypeID order by 房型数量 desc

-- 这里可以做成视图
select tbMain.ID,tbMain.HotelID as 酒店编号 , tbMain.FloorID,tbMain.RoomNumber,tbMain.RoomSize,
B.ModelName , B.Creater, B.BasePrice as 房价, B.CanAddBed as 是否接受加床 , B.BreakfastCount as 早餐数量
from HMS_HotelRoom as tbMain join HMS_RoomModel as B on B.ID = tbMain.RoomTypeID; 

select * from getRoomInfo order by ID; -- 查询视图

select max(ModelName),count(ModelName),max(BasePrice) as 聚合函数最高价
	 from HMS_RoomModel as A group by A.ModelName
	 order by 聚合函数最高价	-- 快速获取组数 max + group (聚合函数)

select modelname,count(modelname) 基础房型数量 
from HMS_RoomModel 
group by modelname 
order by 基础房型数量 desc