﻿
using System;
using System.Text;
using System.Linq;

namespace WhatsNewInCSharp7._0
{

    class Program
    {
        public static int WhatsNewAboutCS7(params object[] paramList)
        {
            int a = 1204;
            int b = a;

            // 新特性 1.ref关键字的新用法
            ref int c = ref a;
            b = (int)Math.Pow(2, 16);
            c = b; // 应该是 a = b = c = 65535 就对了

            Console.WriteLine("a = {0} and b = {1}!", a, b);

            // 新特性 2.局部函数 (类似于lambda) 原理上应该也是开辟一个局部lambda 类型为 Func<T>(...)
            string addString(object obj1st, object obj2nd)
            {
                return new StringBuilder(obj1st.ToString()).AppendFormat(".{0}",obj2nd.ToString()).ToString();
            }

            var s1 = "成都网顺智能软件";
            var s2 = "有限公司";
            Console.WriteLine("{0} + {1}\n\t---> {2}", s1, s2, addString(s1, s2));

            return -1;
        }

        // 3.返回值为元祖的函数
        public static (string oriStr, Int32 LenStr) Foo()
        {
            return ("", 12);
        }

        static void Main(string[] args)
        {
            // 在主函数测试 C# 7.0 新特性 看到了一篇关于VS2017的新特性博客
            // 原文地址 - https://www.ithome.com/html/win10/305148.htm

            WhatsNewAboutCS7();

            var rs = Foo();
            Console.WriteLine(rs);

            // 新特性 4.自动装拆箱
            var a = "shabi";
            if( a is string b)
            {
                Console.WriteLine(b);
            }   

            Console.WriteLine("Hello World!");

            while (true) ;
        }
    }
}
