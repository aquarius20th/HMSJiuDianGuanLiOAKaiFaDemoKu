﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace 深入理解Linq
{
    class Program
    {
        static void Main(string[] args)
        {
            // 例子1 cast<T>() 对集合进行批量类型转换
            ArrayList DataSource = new ArrayList { "Frist","周二","Third" };
            IEnumerable<string> castAry = DataSource.Cast<string>();
            castAry.ToList().ForEach(item => Console.WriteLine(item));

            // 例子2 直接在Linq中进行过滤
            (from string p in DataSource
             select p.Substring(0, 2))
            .ToList()
            .ForEach(item => Console.WriteLine(item));

            // 例子3 用OfType<T>() 进行尝试转换
            ArrayList dataSource2 = new ArrayList { 1, "质数",1,2,3,5, };
            IEnumerable<int> objs = dataSource2.OfType<int>();
            foreach (var iteim in objs)
                Console.Write(iteim);

            while (true) ;
        }
    }
}
